const http = require('http')
var mosca = require('mosca');
var settings = {
    port:1883
}

var server = new mosca.Server(settings);

server.on('ready', function(){
    console.log("ready");
})

// fired when a message is received
server.on('published', function(packet, client) {
    console.log('Published: ', packet.payload.toString('utf8') + ' on ' + packet.topic);
    if (packet.topic == "score") {
        try {
            let message = JSON.parse(packet.payload.toString('utf8'))
            const options = {
                hostname: 'localhost',
                port: 3000,
                path: '/add_points',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            data = {"container": message.container, "points" : message.points}
            data = JSON.stringify(data)
            const req = http.request(options, function(res) {
                res.setEncoding('utf8')
                
                res.on('data', function (chunk) {
                    console.log(chunk)
                })
            })
            req.on('error', function (error) {
                console.log(error.message)
                req.end()
            })
            req.write(data)
            req.end()
        } catch (error) {
            console.log('error happended')
        }
    } else if (packet.topic == "button") {
        try {
            let message = JSON.parse(packet.payload.toString('utf8'))
            const options = {
                hostname: 'localhost',
                port: 3000,
                path: '/container_btn_pressed',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            data = {"name": message.container}
            data = JSON.stringify(data)
            const req = http.request(options, function(res) {
                res.setEncoding('utf8')
                
                res.on('data', function (chunk) {
                    console.log(chunk)
                })
            })
            req.on('error', function (error) {
                console.log(error.message)
                req.end()
            })
            req.write(data)
            req.end()
        } catch (error) {
            console.log('error happended')
        }
    } else if (packet.topic == "state") {
        try {
            let message = JSON.parse(packet.payload.toString('utf8'))
            const options = {
                hostname: 'localhost',
                port: 3000,
                path: '/idle',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            data = {"container": message.container}
            data = JSON.stringify(data)
            const req = http.request(options, function(res) {
                res.setEncoding('utf8')
                
                res.on('data', function (chunk) {
                    console.log(chunk)
                })
            })
            req.on('error', function (error) {
                console.log(error.message)
                req.end()
            })
            req.write(data)
            req.end()
        } catch (error) {
            console.log('error happended')
        }
    }
})