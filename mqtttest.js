var mqtt = require('mqtt');
var client  = mqtt.connect('mqtt://localhost:1883');
client.on('connect', function () {
   console.log('Connected to the MQTT broker at: ' + 'mqtt://localhost:1883' )
});

function sendMessage(value) {
    client.publish('state', value + "");
}

var standard_input = process.stdin;
standard_input.setEncoding('utf-8')
standard_input.on('data', function (data) {
    sendMessage(data)
});