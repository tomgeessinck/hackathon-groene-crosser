let io = null

var mysql = require('mysql')
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: ""
})
const database_name = "groene_crosser"

con.connect(function(err) {
    if (err) throw err
    console.log("Connected!")
    con.query("CREATE DATABASE IF NOT EXISTS " + database_name, function (err, result) {
        if (err) throw err
        console.log("Database created or already existed!")
        con.changeUser({database : database_name}, function(err) {
            if (err) throw err
            con.query("CREATE TABLE IF NOT EXISTS users(name varchar(255) primary key not null, points int default 0, container varchar(255) default null)", function (err, result) {
                if (err) throw err
                console.log("Table users created or already existed!")
            })
            con.query("CREATE TABLE IF NOT EXISTS containers(name varchar(255) primary key not null, pressed boolean default false, heigth int default null)", function (err, result) {
                if (err) throw err
                console.log("Table containers created or already existed!")
            })
        })
    })
})

const express = require('express')
var bodyParser = require("body-parser")
const path = require('path')
const app = express()
const port = 3000



var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://localhost:1883')
client.on('connect', function () {
   console.log('Connected to the MQTT broker at: ' + 'mqtt://localhost:1883' )
})
function sendMQTTMessage(value) {
    client.publish('qr_scanned', value + "")
}
app.use(express.static(__dirname + '/views'));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/views/test.html'))
})

app.get('/top10', function(req, res) {
    con.query("SELECT name, points FROM users ORDER BY points DESC limit 10", function (err, result) {
        if (err) throw err
        res.send(JSON.stringify(result))
    })
})

app.post('/name', function(req, res) {
    var name = req.body.name
    var container = req.body.container

    con.query("SELECT * FROM containers WHERE name = '" + container + "'", function (err, result) {         
        if (result[0] !== undefined && result[0].pressed == 1) {
            con.query("UPDATE containers SET pressed = 0 WHERE name = '" + container + "'", function (err, result) {
                if (err) throw err
                console.log("Unpressed the button on container: " + container)
            })
            con.query("SELECT * FROM users WHERE name = '" + name + "'", function (err, result) { 
                if (err) throw err
                if (JSON.stringify(result) == "[]") {
                    con.query("INSERT INTO users (name, container) VALUES ('" + name + "','" + container + "')", function (err, result) {
                        if (err) throw err
                        console.log("Name: " + name + " inserted into the database")
                        
                        sendMQTTMessage("scanned")
                        io.sockets.emit("start", container + "")
                    })
                    res.end("Completed")
                } else {
                    con.query("UPDATE users SET container = '" + container + "' WHERE name = '" + name + "'", function (err, result) {
                        if (err) throw err
                        console.log("Assigned container to user with the name: " +  name)
                        
                        sendMQTTMessage("scanned")
                        io.sockets.emit("start", container + "")
                    })
                    res.end("Completed")
                }
            })    
        } else {
            res.end("Knop is nog niet ingedrukt!")
            console.log("Knop is nog niet ingedrukt!")
        }
    })
})

app.post('/add_points', function(req, res) {
    var container = req.body.container
    var points = req.body.points
    let oldPoints = 0
    con.query("SELECT * FROM users WHERE container = '" + container + "'", function (err, result) {
        if (err || result[0] == undefined) {
            console.log('No one is near that container')
            res.end("No one is near that container")
        } else {
            oldPoints = parseInt(result[0].points)
            name = result[0].name

            if(points < 0) {

                console.log("Points taken away");
                
                io.sockets.emit("hufter", container + "")
            } else {

                console.log("Points added");
                io.sockets.emit("message", container + "")
            }



            con.query("UPDATE users SET points=" + (oldPoints + parseInt(points)) + " WHERE container = '" + container + "'", function (err, result) {
                if (err) throw err
                console.log("Updated the points of " + name + " at container: " + container + " , from : " + oldPoints + " to: " + (oldPoints + parseInt(points)))
                res.end("Completed")
            })
        }
    })
})

app.post('/container_btn_pressed', function(req, res) {
    var name = req.body.name
    con.query("SELECT * FROM containers WHERE name = '" + name + "'", function (err, result) {
        if (err || result[0] == undefined ) {
            console.log('That container doesnt exist')
            res.end("That container doesnt exist")

            //WORK AROUND
            con.query("INSERT INTO containers (name) VALUES ('" + name + "')", function (err, result) {
                if (err) throw err
                console.log("containers: " + name + " inserted into the database")
            })
            con.query("UPDATE containers SET pressed = 1 WHERE name = '" + name + "'", function (err, result) {
                if (err) throw err
                console.log("Updated the container " + name + ", button had been pressed")
                res.end("Completed")
            })
            
        } else {
            if (result[0].pressed == 1) {
                res.end("Knop is al ingedrukt!")
            } else {
                con.query("UPDATE containers SET pressed = 1 WHERE name = '" + name + "'", function (err, result) {
                    if (err) throw err
                    console.log("Updated the container " + name + ", button had been pressed")
                    res.end("Completed")
                })
            }
        }
    })
})

app.post('/idle', function(req, res) {
    var container = req.body.container
    con.query("UPDATE containers SET pressed = 0 WHERE name = '" + container + "'", function (err, result) {
        if (err) throw err
        console.log("Updated the container " + container + ", button had been UNpressed")
        con.query("UPDATE users SET container = null WHERE container = '" + container + "'", function (err, result) {
            if (err) throw err
            console.log("Updated users container with name  " + container + ", has been undone/finsished")
            
            io.sockets.emit("messageend", container + "")
            res.end("Completed")
        })
    })
})

let server = app.listen(port, () => console.log(`Example app listening on port ${port}!`))
io = require('socket.io')(server);

